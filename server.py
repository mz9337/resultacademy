import os
import urllib.request
import json
#from os import listdir
from os.path import isfile
from flask import Flask, flash, request, redirect, render_template, jsonify
import numpy as np
import cv2
from keras.models import Sequential, load_model


ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
#runs the script
app = Flask(__name__)

MODEL_NAME = "azure_model_filters1"

model = load_model(MODEL_NAME+".h5")
model._make_predict_function()

#starts when POST method is received
@app.route('/', methods=['GET'])
def index():
    return "test"


@app.route('/is_object', methods=['POST'])
def is_object():
    if request.method == 'POST':

        #grabs file named hololens (image from lens)
        # print(request.files)
        # file = request.files['hololens.jpg']

        if 'hololens' not in request.files:
            return "rrr"

        file = request.files['hololens']

        filename = file.filename # save file 
        # filepath = os.path.join("/", filename);

        file.save(filename)
        img = cv2.imread(filename)


        img = cv2.resize(img, (400,300))

        print("tuki")

        prediction = model.predict(np.array([img]))[0][0]
        print(prediction)

        return str(prediction)

        # cv2.imshow("test", img)
        # cv2.waitKey(0)

        # toReturn = {"objekt" : "false", "objektOK": "false"}

        # #during running i create file in terminal which means object is recognized and fits
        # if isfile("objektOK"):
        #     toReturn = {"objekt" : "true", "objektOK": "true"}
        # #during running i create file in terminal which means object is recognized and fits
        # elif isfile("objekt"):
        #     toReturn = {"objekt" : "true", "objektOK": "false"}

        # #kinda useless right now, since we dont forward image
        # try:
        #     im = Image.open(file)
        #     im.thumbnail(size)
        #     im.save("thumbnail", "JPEG", dpi=(size[0], size[1]))
        #     imgSize = im.size
        #     px = im.load()
        #     stolpec = list()
        #     for y in range(imgSize[1]):
        #         vrstica = list()
        #         for x in range(imgSize[0]):
        #             vrstica.append(px[x,y])
        #         stolpec.append(vrstica)
        # except IOError:
        #     print ("cannot create thumbnail for", filename)

        # im.close()
        # file.close()

        # #returns correct json to hololens for display
        # response = json.dumps(toReturn)
        # return response


# #runs the app
# if __name__ == "__main__":
#     app.run(host='0.0.0.0', port=8080, debug=True)
